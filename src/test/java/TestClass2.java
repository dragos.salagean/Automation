import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by salagean on 25/08/2016.
 */

@Test
public class TestClass2 extends BaseTest {


    public void test1(){
        System.out.println("this is test 1");
        Assert.assertEquals("String2", "String2", "checking that strings are equal");
        Assert.assertEquals(1,2,"checking that numbers are equal");
        Assert.assertTrue(true,"checking boolean");

    }

    public void test2(){
        System.out.println("this is test 2");
        throw new RuntimeException();

    }
    @Test(enabled =  false, description = "this is test 3 that does nothing", groups = "smoke")
    private void test3(){
        System.out.println("this is test 3");
    }

}
